extends KinematicBody2D

#Const
const acceleration=300
const MaxSpeed=50
const friction=300

#Variables
var velocity=Vector2.ZERO

func _physics_process(delta):
	movement(delta)

func movement(delta):
	#Movement
	var input_vector=Vector2.ZERO
	input_vector.x=Input.get_action_strength("ui_right")-Input.get_action_strength("ui_left")
	input_vector.y=Input.get_action_strength("ui_down")-Input.get_action_strength("ui_up")
	input_vector=input_vector.normalized()

	if input_vector != Vector2.ZERO:
		velocity=velocity.move_toward(input_vector*MaxSpeed,acceleration*delta)
	else:
		velocity=velocity.move_toward(Vector2.ZERO,friction*delta)

	velocity=move_and_slide(velocity)
